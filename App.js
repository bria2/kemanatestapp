import { AuthNavigator } from "./source/navigations/AuthNavigator";

export default function App() {
  return <AuthNavigator />;
}
