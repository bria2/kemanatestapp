# kemanaTestApp

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/bria2/kemanatestapp.git
git branch -M main
git push -uf origin main
```

## How to run / use project

Create project with expo and this app is for both iOS and android

After clone this repositories fire some commands for run the App

- yarn install
- expo start or (you can directly run in individuals platform with below command)
- yarn ios or yarn android

## Project Description

## Splash Screen

- First the splash screen is loading
- After that navigate to the Product screen

## Product Screen

- In Product screen header set a title of screen and cart icon
- There is showing some 10 products name , price and addToCart button
- On click of addToCart button particular item will be added in cart
- On cart icon show the quantity and click on cart icon navigate to the cart screen

## Cart Screen

- In Cart screen header set a title of screen and clear all button
- On Clear All button click delete all items of cart and empty cart
- Showing item which is added in cart
- In this item showing product image, name , price , quantity increase and decrease and delete item icon
- On click of plus (+) button it's increase quantity of particular product
- On click of Minus (-) button it's decrease quantity of particular product
- On click of delete button it's delete particular product from cart

  ## Cart Summary

  - In this showing the total price of all item
  - Also showing shipping fee which is fixed $5
  - Also showing tax of particular product
  - And there is Sub total of total price , tax and shipping fees

  - There is one another Pay Now button
  - It's simple redirect to the Successfully place order

## Place Order Screen

- Showing simple successfully Place order message
- And Continue shopping button is there
- It's navigate to Product screen

## Library

- I have used navigation library for moving between screens
- And i have used Async storage library for store data locally and RN Async component is deprecated so i have to used library.
