import React from "react";

//Navigation library need to add for navigate one screen to another
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

//import all screen name from constant file
import {
  CART_VIEW,
  PLACE_ORDER,
  PRODUCT,
  SPLASH_SCREEN,
} from "../constants/screens";

//import all pages
import Splash from "../modules/Splash/ui";
import Cart from "../modules/Cart/UI";
import { CartProvider } from "../modules/Cart/CartContext";
import PlaceOrder from "../modules/PlaceOrder/ui";
import ProductPage from "../modules/Product/ui";

//Create a stack with use of createNativeStackNavigator
const Stack = createNativeStackNavigator();
export const AuthNavigator = () => {
  return (
    <CartProvider>
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen name={SPLASH_SCREEN} component={Splash} />
          <Stack.Screen name={PRODUCT} component={ProductPage} />
          <Stack.Screen name={CART_VIEW} component={Cart} />
          <Stack.Screen name={PLACE_ORDER} component={PlaceOrder} />
        </Stack.Navigator>
      </NavigationContainer>
    </CartProvider>
  );
};
