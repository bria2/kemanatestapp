import React, { useContext, useEffect, useState } from "react";

import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  FlatList,
} from "react-native";

//All Files imported here
import { colors } from "../../../constants/colors";
import string from "../../../constants/string";
import EmptyCartIcon from "../../../../assets/empty_cart.png";
import { shadows } from "../../../constants/shadows";
import { CartContext } from "../CartContext";
import { CartProduct } from "../../../components/CartProduct";
import Header from "../../../components/common/Header";
import Button from "../../../components/common/Button";
import { PLACE_ORDER, PRODUCT } from "../../../constants/screens";
import { GLOBAL } from "../../../constants/common";
import { CommonActions } from "@react-navigation/native";

//Cart Screen
const Cart = ({ navigation }) => {
  const {
    items,
    getTotalPrice,
    getSubTotalPrice,
    deleteItem,
    deleteAllItem,
    incQuantity,
    decQuantity,
    getTotalTax,
  } = useContext(CartContext);
  const [productData, setProductData] = useState([]);

  //use Effect for get product data
  useEffect(() => {
    setProductData(items);
  }, [items]);

  //rendering the cart product item view
  const renderRow = ({ item }) => {
    return (
      <CartProduct
        {...item}
        onIncrementPress={() => incQuantity(item.id)}
        qty={item.qty}
        onDeleteSelectPress={() => deleteItem(item.id)}
        onDecrementPress={() => decQuantity(item.id)}
      />
    );
  };

  //rendering the footer component view
  const ListFooterComponent = () => {
    return (
      <View
        style={{
          paddingTop: 10,
        }}
      >
        <Text style={[styles.headerTitle, { paddingLeft: 10 }]}>
          {string.cartSummary}
        </Text>
        <View style={[styles.cartSummaryBlockStyle, shadows.card]}>
          <View>
            <Text style={styles.summaryTextTitle}>{string.subtotal} :</Text>
            <Text style={styles.summaryTextTitle}>{string.tax} :</Text>
            <Text style={styles.summaryTextTitle}>{string.shipping} :</Text>
            <Text style={[styles.summaryTextTitle, { fontWeight: "bold" }]}>
              {string.total} :
            </Text>
          </View>
          <View style={{ paddingRight: 10 }}>
            <Text style={styles.summaryTextTitle}>$ {getSubTotalPrice()}</Text>
            <Text style={styles.summaryTextTitle}>$ {getTotalTax()}</Text>
            <Text style={styles.summaryTextTitle}>$ {GLOBAL.SHIPPING_FEE}</Text>
            <Text style={[styles.summaryTextTitle, { fontWeight: "bold" }]}>
              $ {getTotalPrice()}
            </Text>
          </View>
        </View>
        <Button
          title={string.payNow}
          onButtonPress={() => navigation.navigate(PLACE_ORDER)}
        />
      </View>
    );
  };

  //render header and flatlist for product list
  return (
    <SafeAreaView style={styles.container}>
      <Header
        title={string.cart}
        isBack
        isEmpty={productData.length > 0 ? false : true}
        onBackPress={() => navigation.goBack()}
        onDeletePress={() => deleteAllItem()}
      />
      {productData.length > 0 ? (
        <FlatList
          data={productData}
          renderItem={renderRow}
          contentContainerStyle={{ flexGrow: 1 }}
          showsVerticalScrollIndicator={false}
          ListFooterComponent={ListFooterComponent}
          keyExtractor={(item) => item.product.id.toString()}
        />
      ) : (
        <View style={styles.emptyCartBlockStyle}>
          <Image
            source={EmptyCartIcon}
            style={{ height: 300, resizeMode: "contain" }}
          />
          <Button
            title={string.continueShopping}
            onButtonPress={() =>
              navigation.dispatch(
                CommonActions.reset({
                  index: 1,
                  routes: [{ name: PRODUCT }],
                })
              )
            }
          />
        </View>
      )}
    </SafeAreaView>
  );
};

//Cart Styling
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.offWhite,
    paddingTop: 10,
  },
  headerTitle: {
    fontSize: 20,
    color: colors.primary,
    fontWeight: "600",
  },
  summaryTextTitle: {
    fontSize: 17,
    color: colors.primary,
    fontWeight: "400",
    height: 30,
  },
  cartSummaryBlockStyle: {
    margin: 8,
    paddingTop: 10,
    paddingLeft: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: colors.white,
    borderRadius: 10,
  },
  emptyCartBlockStyle: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});

export default Cart;
