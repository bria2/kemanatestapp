import React, { createContext, useEffect, useState } from "react";
import { GLOBAL } from "../../constants/common";
import { getProduct } from "../../JSON/ProductData";
import { getData, removeData, storeData } from "../../Utils/Helper";
export const CartContext = createContext();

//Create a Cart Provide with use of Create Context
export const CartProvider = (props) => {
  const [items, setItems] = useState([]);

  //This use Effect is for store data in local storage
  useEffect(() => {
    if (items?.length != 0) storeData("cart_data", items);
  }, [items]);

  //This use Effect is for get data from local storage
  useEffect(() => {
    getData("cart_data").then((data) => {
      data && setItems(data);
    });
  }, []);

  //This function for add item in cart
  const addItemToCart = (id) => {
    const product = getProduct(id);
    setItems((prevItems) => {
      const item = prevItems?.find((item) => item.id == id);
      if (!item) {
        const newItemArr = [
          ...prevItems,
          {
            id,
            qty: 1,
            product,
            totalPrice: product.productPrice,
            tax: getProductTax(Number(product.productPrice)),
          },
        ];
        return newItemArr;
      } else {
        const existingItemArr = prevItems?.map((item) => {
          if (item.id == id) {
            item.qty++;
            item.totalPrice =
              Number(item.totalPrice) + Number(product.productPrice);
            item.tax =
              Number(item.tax) + getProductTax(Number(product.productPrice));
          }
          return item;
        });
        return existingItemArr;
      }
    });
  };

  //This function for delete single item from cart
  const deleteItem = (id) => {
    const deletedItem = items.filter((item) => item.id !== id);
    storeData("cart_data", deletedItem);
    setItems(deletedItem);
  };

  //This function for increase quantity of single item from cart
  const incQuantity = (id) => {
    const updatedQty = items.filter((item) => {
      if (item.id === id) {
        item.qty++;
        item.totalPrice =
          Number(item.totalPrice) + Number(item?.product?.productPrice);
        item.tax =
          Number(item.tax) + getProductTax(Number(item?.product?.productPrice));
        return item;
      }
      return items;
    });
    storeData("cart_data", updatedQty);
    setItems(updatedQty);
  };

  //This function for decrease quantity of single item from cart
  const decQuantity = (id) => {
    const updatedQty = items.filter((item) => {
      if (item.id === id) {
        if (item.qty > GLOBAL.MIN_QTY) {
          item.qty--;
          item.totalPrice =
            Number(item.totalPrice) - Number(item?.product?.productPrice);
          item.tax =
            Number(item.tax) -
            getProductTax(Number(item?.product?.productPrice));
          return item;
        }
      }
      return items;
    });
    storeData("cart_data", updatedQty);
    setItems(updatedQty);
  };

  //This function for delete all item from cart
  const deleteAllItem = () => {
    removeData("cart_data");
    setItems([]);
  };

  //This function for get item count from cart
  const getItemsCount = () => {
    return items?.reduce((sum, item) => sum + item.qty, 0);
  };

  //This function for get subtotal price from cart item
  const getSubTotalPrice = () => {
    const subTotal = items?.reduce(
      (sum, item) => Number(sum) + Number(item.totalPrice),
      0
    );
    return subTotal?.toFixed(2);
  };

  //This function for get product tax
  const getProductTax = (price) => {
    const tax = (price * GLOBAL.TAX) / 100;
    return tax;
  };

  //This function for get total tax
  const getTotalTax = () => {
    const totalTax = items?.reduce(
      (sum, item) => Number(sum) + Number(item.tax),
      0
    );
    return totalTax?.toFixed(2);
  };

  //This function for get total price
  const getTotalPrice = () => {
    const subTotal = getSubTotalPrice();
    const tax = getTotalTax();
    const total = Number(GLOBAL.SHIPPING_FEE) + Number(subTotal) + Number(tax);
    return total?.toFixed(2);
  };

  return (
    <CartContext.Provider
      value={{
        items,
        setItems,
        getItemsCount,
        addItemToCart,
        getTotalPrice,
        deleteItem,
        deleteAllItem,
        incQuantity,
        decQuantity,
        getSubTotalPrice,
        getTotalTax,
      }}
    >
      {props.children}
    </CartContext.Provider>
  );
};
