import React, { useContext } from "react";

import { SafeAreaView, StyleSheet, Image, Text } from "react-native";

//All Files imported here
import { colors } from "../../../constants/colors";
import { PRODUCT } from "../../../constants/screens";
import string from "../../../constants/string";
import SuccessIcon from "../../../../assets/order_placed.png";
import Button from "../../../components/common/Button";
import { CartContext } from "../../Cart/CartContext";
import { CommonActions } from "@react-navigation/native";

//Successfully place order class
const PlaceOrder = ({ navigation }) => {
  const { deleteAllItem } = useContext(CartContext);

  //continue press function is navigation to main product list
  const onContinuePress = () => {
    deleteAllItem();
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{ name: PRODUCT }],
      })
    );
  };

  //Design Place order view
  return (
    <SafeAreaView style={styles.container}>
      <Image source={SuccessIcon} style={{ height: 200, width: 200 }} />
      <Text style={styles.textStyle}>{string.orderPlaceSuccessfully}</Text>

      <Button
        title={string.continueShopping}
        onButtonPress={() => onContinuePress()}
      />
    </SafeAreaView>
  );
};

//Place order styling
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: "center",
    justifyContent: "center",
  },
  textStyle: {
    fontSize: 30,
    color: colors.primary,
    fontWeight: "bold",
    fontStyle: "italic",
    marginTop: 20,
    textAlign: "center",
  },
});

export default PlaceOrder;
