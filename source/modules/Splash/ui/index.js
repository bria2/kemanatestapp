import React, { useEffect } from "react";

import { SafeAreaView, StyleSheet, Image, Text } from "react-native";
import { CommonActions } from "@react-navigation/native";

//All files imported here
import { colors } from "../../../constants/colors";
import { PRODUCT } from "../../../constants/screens";
import string from "../../../constants/string";

//Splash screen
const Splash = ({ navigation }) => {
  //This useEffect for wait for some second and navigate to Product screen
  useEffect(() => {
    setTimeout(() => {
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{ name: PRODUCT }],
        })
      );
    }, 1500);
  }, []);

  //Splash screen design
  return (
    <SafeAreaView style={styles.container}>
      <Image
        source={require("../../../../assets/gif/splash.gif")}
        style={styles.logoStyle}
      />
      <Text style={styles.textStyle}>{string.Title}</Text>
    </SafeAreaView>
  );
};

//Splash screen styling
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: "center",
    justifyContent: "center",
  },
  textStyle: {
    fontSize: 30,
    color: colors.primary,
    fontWeight: "bold",
    fontStyle: "italic",
    marginTop: 10,
  },
  logoStyle: {
    width: 300,
    height: 300,
  },
});

export default Splash;
