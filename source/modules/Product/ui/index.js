import React, { useEffect, useState, useContext } from "react";

import { SafeAreaView, StyleSheet, FlatList } from "react-native";

//All Files imported here
import { colors } from "../../../constants/colors";
import string from "../../../constants/string";
import { CART_VIEW } from "../../../constants/screens";
import { Product } from "../../../components/Product";
import { getProducts } from "../../../JSON/ProductData";
import Header from "../../../components/common/Header";
import { Loading } from "../../../components/common/Loading";
import { CartContext } from "../../Cart/CartContext";

//Main product List page
const ProductPage = ({ navigation }) => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);

  const { addItemToCart } = useContext(CartContext);

  //This useEffect is for get products from data and set in state
  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setProducts(getProducts());
    }, 600);
  }, [products]);

  //This function is for Add item in cart
  const onAddToCart = (id) => {
    setLoading(true);
    setTimeout(() => {
      addItemToCart(id);
      setLoading(false);
    }, 600);
  };

  //Rendering product item row
  const renderRow = ({ item: product }) => {
    return (
      <Product {...product} onAddToCartPress={() => onAddToCart(product.id)} />
    );
  };

  //Design for header and product flatlist
  return (
    <SafeAreaView style={styles.container}>
      <Header
        title={string.Title}
        isCart
        onCartPress={() => navigation.navigate(CART_VIEW)}
      />
      <FlatList
        data={products}
        renderItem={renderRow}
        contentContainerStyle={{ flexGrow: 1 }}
        numColumns={2}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item) => item.id.toString()}
      />
      {loading && <Loading transparent />}
    </SafeAreaView>
  );
};

//Product page styling
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.offWhite,
    paddingTop: 10,
  },
  productContainerStyle: {
    height: 300,
    width: "50%",
    alignItems: "center",
    justifyContent: "center",
  },
  productBlockStyle: {
    height: 265,
    width: "90%",
    borderRadius: 10,
    alignItems: "center",
    backgroundColor: colors.white,
  },
  productTextStyle: {
    marginTop: 10,
    color: colors.primary,
    fontWeight: "bold",
    fontSize: 15,
  },
  addToCartBlockStyle: {
    backgroundColor: colors.primary,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    height: 45,
    position: "absolute",
    bottom: 0,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  addToCartTextStyle: {
    color: colors.white,
    fontWeight: "bold",
    fontSize: 16,
    fontStyle: "italic",
  },
});

export default ProductPage;
