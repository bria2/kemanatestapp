//Need to import this library for store data locally and default RN Async storage component is deprecated
import AsyncStorage from "@react-native-async-storage/async-storage";

//store Async storage data by key and value
export const storeData = async (key, value) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(key, jsonValue);
  } catch (e) {
    // saving error
    console.log("Error", e);
  }
};

//get Async storage data by key
export const getData = async (key) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    // error reading value
    console.log("Error", e);
  }
};

//remove Async storage data by key
export const removeData = async (key) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (e) {
    // remove error
    console.log("Error", e);
  }
};

//remove all Async storage data
export const clearAll = async () => {
  try {
    await AsyncStorage.clear();
  } catch (e) {
    // clear error
    console.log("Error", e);
  }
};
