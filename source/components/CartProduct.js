import React from "react";

import { StyleSheet, View, Text, TouchableOpacity, Image } from "react-native";
import { colors } from "../constants/colors";
import { shadows } from "../constants/shadows";
import string from "../constants/string";
import DeleteIcon from "../../assets/delete_icon.png";

//Cart product item component
export const CartProduct = ({
  product,
  qty,
  onIncrementPress,
  onDecrementPress,
  onDeleteSelectPress,
}) => {
  return (
    <View style={[styles.cartBlockStyle, shadows.card]}>
      <Image
        source={{
          uri: product.productImage,
        }}
        style={styles.footImageStyle}
      />

      <View style={{ borderWidth: 0, width: "50%" }}>
        <Text style={styles.productTitleStyle}>{product.productName}</Text>
        <Text style={styles.productPriceStyle}>$ {product.productPrice}</Text>
        <View style={styles.plusMinusBlockStyle}>
          <TouchableOpacity
            style={styles.plusMinusIconBlockStyle}
            onPress={onIncrementPress}
          >
            <Text style={styles.plusMinusIcon}>+</Text>
          </TouchableOpacity>
          <Text style={[styles.quantityTextSTyle]}>{qty}</Text>
          <TouchableOpacity
            style={[styles.plusMinusIconBlockStyle, styles.minusIconBlockStyle]}
            onPress={onDecrementPress}
          >
            <Text style={styles.plusMinusIcon}>-</Text>
          </TouchableOpacity>
        </View>
      </View>
      <TouchableOpacity
        style={{ marginTop: 10, marginLeft: 6 }}
        onPress={onDeleteSelectPress}
      >
        <Image source={DeleteIcon} style={styles.deleteIconStyle} />
      </TouchableOpacity>
    </View>
  );
};

//Cart product styling
const styles = StyleSheet.create({
  deleteIconStyle: {
    height: 20,
    width: 30,
    resizeMode: "contain",
  },
  cartSummaryBlockStyle: {
    margin: 8,
    paddingTop: 10,
    paddingLeft: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: colors.white,
    borderRadius: 10,
  },
  productTitleStyle: {
    fontSize: 18,
    color: colors.primary,
    fontWeight: "bold",
  },
  productPriceStyle: {
    marginTop: 6,
    fontSize: 16,
    color: colors.primary,
  },
  plusMinusBlockStyle: {
    height: 30,
    borderWidth: 0.5,
    borderRadius: 10,
    width: 100,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderColor: colors.primary,
    marginTop: 8,
  },
  plusMinusIconBlockStyle: {
    width: 25,
    height: 30,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    backgroundColor: colors.primary,
    alignItems: "center",
    justifyContent: "center",
  },
  minusIconBlockStyle: {
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  plusMinusIcon: {
    fontSize: 20,
    color: colors.white,
  },
  quantityTextSTyle: {
    fontWeight: "600",
    color: colors.primary,
    fontSize: 17,
  },
  cartBlockStyle: {
    borderRadius: 10,
    margin: 5,
    flexDirection: "row",
    backgroundColor: colors.white,
    justifyContent: "space-between",
    padding: 10,
  },
  footImageStyle: {
    height: 90,
    width: 100,
    borderRadius: 10,
  },
});
