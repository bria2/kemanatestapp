import React, { useEffect } from "react";

import { StyleSheet, View, Text, TouchableOpacity, Image } from "react-native";
import { colors } from "../constants/colors";
import { shadows } from "../constants/shadows";
import string from "../constants/string";

//Main product list item
export const Product = ({
  productName,
  productPrice,
  productImage,
  onAddToCartPress,
}) => {
  useEffect(() => {
    Image.prefetch(productImage);
  }, []);

  return (
    <View style={styles.productContainerStyle}>
      <View style={[styles.productBlockStyle, shadows.card]}>
        <Image
          style={styles.productImageStyle}
          defaultSource={require("../../assets/default_image.png")}
          source={{
            uri: productImage,
            cache: "only-if-cached",
            headers: {
              Accept: "*/*",
            },
          }}
        />
        <Text style={styles.productTextStyle}>{productName}</Text>
        <Text style={styles.productTextStyle}>$ {productPrice}</Text>
        <TouchableOpacity
          style={styles.addToCartBlockStyle}
          onPress={onAddToCartPress}
        >
          <Text style={styles.addToCartTextStyle}>{string.addToCart}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

//Product styling
const styles = StyleSheet.create({
  productImageStyle: {
    height: 160,
    width: "100%",
    borderRadius: 10,
  },
  productContainerStyle: {
    height: 280,
    width: "50%",
    alignItems: "center",
    justifyContent: "center",
  },
  productBlockStyle: {
    paddingBottom: 20,
    width: "90%",
    height: 260,
    borderRadius: 10,
    alignItems: "center",
    backgroundColor: colors.white,
  },
  productTextStyle: {
    marginTop: 10,
    color: colors.primary,
    fontWeight: "bold",
    fontSize: 15,
  },
  addToCartBlockStyle: {
    backgroundColor: colors.primary,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    position: "absolute",
    bottom: 0,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  addToCartTextStyle: {
    color: colors.white,
    fontWeight: "bold",
    fontSize: 16,
    fontStyle: "italic",
  },
});
