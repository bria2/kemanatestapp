import React, { useContext } from "react";

import { StyleSheet, View, Text, TouchableOpacity, Image } from "react-native";
import { colors } from "../../constants/colors";
import BackIcon from "../../../assets/back_arrow.png";
import CartIcon from "../../../assets/cart_icon.png";
import { CartContext } from "../../modules/Cart/CartContext";
import string from "../../constants/string";

//Common header component for app header
const Header = ({
  title,
  isBack,
  onBackPress,
  isEmpty,
  isCart,
  onCartPress,
  onDeletePress,
}) => {
  const { getItemsCount } = useContext(CartContext);

  return (
    <View style={styles.headerBlockStyle}>
      <>
        {isBack && (
          <TouchableOpacity
            style={{ flexDirection: "row", alignItems: "center" }}
            onPress={onBackPress}
          >
            <Image source={BackIcon} style={styles.backArrowStyle} />
          </TouchableOpacity>
        )}
        <Text style={styles.headerTitle}>{title}</Text>
      </>

      {!isEmpty && !isCart && (
        <TouchableOpacity
          style={styles.deleteAllBlockStyle}
          onPress={onDeletePress}
        >
          <Text style={[styles.countTextStyle, { fontWeight: "bold" }]}>
            {string.clearAll}
          </Text>
        </TouchableOpacity>
      )}

      {isCart && (
        <TouchableOpacity onPress={onCartPress} style={styles.cartBlockStyle}>
          <Image
            source={CartIcon}
            style={{ height: 35, width: 35, tintColor: colors.primary }}
          />
          {!getItemsCount() == 0 && (
            <View style={styles.itemCountBlockStyle}>
              <Text style={styles.countTextStyle}>{getItemsCount()}</Text>
            </View>
          )}
        </TouchableOpacity>
      )}
    </View>
  );
};

//Header styling
const styles = StyleSheet.create({
  itemCountBlockStyle: {
    position: "absolute",
    left: 10,
    bottom: 22,
    backgroundColor: colors.primary,
    borderRadius: 11.5,
    height: 23,
    width: 23,
    justifyContent: "center",
  },
  countTextStyle: {
    fontSize: 14,
    color: colors.white,
    textAlign: "center",
  },
  headerBlockStyle: {
    alignItems: "center",
    padding: 10,
    borderBottomWidth: 0.4,
    flexDirection: "row",
  },
  headerTitle: {
    fontSize: 20,
    color: colors.primary,
    fontWeight: "600",
  },
  backArrowStyle: {
    height: 40,
    width: 30,
    tintColor: colors.primary,
  },
  deleteAllBlockStyle: {
    marginTop: 10,
    marginLeft: 6,
    right: 10,
    position: "absolute",
    padding: 8,
    borderRadius: 8,
    backgroundColor: colors.primary,
  },
  cartBlockStyle: {
    marginTop: 10,
    marginLeft: 6,
    right: 10,
    position: "absolute",
  },
  deleteIconStyle: {
    height: 20,
    width: 30,
    resizeMode: "contain",
  },
});

export default Header;
