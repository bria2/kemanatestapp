import React from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";
import { colors } from "../../constants/colors";

//Common Loading component for activity indicator
const Loading = ({ transparent = false }) => (
  <View
    style={[
      styles.indicatorBlockStyle,
      { backgroundColor: transparent ? "transparent" : "#fff" },
    ]}
  >
    <ActivityIndicator
      size="large"
      color={colors.primary}
      style={styles.indicatorStyle}
    />
  </View>
);

//Loading styling
const styles = StyleSheet.create({
  indicatorBlockStyle: {
    flex: 1,
    position: "absolute",
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
  },
  indicatorStyle: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
export { Loading };
