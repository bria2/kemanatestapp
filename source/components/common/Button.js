import React from "react";

import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { colors } from "../../constants/colors";
import { shadows } from "../../constants/shadows";

//Common button for touchable event
const Button = ({ title, onButtonPress }) => {
  return (
    <TouchableOpacity
      style={[styles.payNowBtnBlockStyle, shadows.card]}
      onPress={onButtonPress}
    >
      <Text style={styles.payNowTextStyle}>{title}</Text>
    </TouchableOpacity>
  );
};

//Button Styling
const styles = StyleSheet.create({
  payNowBtnBlockStyle: {
    height: 60,
    width: "80%",
    alignSelf: "center",
    marginTop: 15,
    borderRadius: 10,
    backgroundColor: colors.primary,
    justifyContent: "center",
  },
  payNowTextStyle: {
    fontSize: 20,
    color: colors.white,
    textAlign: "center",
    fontWeight: "bold",
  },
});

export default Button;
