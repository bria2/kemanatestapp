//Here is the Product Data List
const PRODUCTS = [
  {
    id: 1,
    productImage:
      "https://media.istockphoto.com/photos/pastel-blue-sneakers-on-crossed-legs-casual-footwear-picture-id1180837581?b=1&k=20&m=1180837581&s=170667a&w=0&h=DkTMmNG3WmUiztzkWbNbn2E10_jMaakoyQO0VgHxIxs=",
    productName: "Nike Air Max 200",
    productPrice: "240.00",
  },
  {
    id: 2,
    productImage:
      "https://media.istockphoto.com/photos/ruby-slippers-for-baby-girl-picture-id455616133?b=1&k=20&m=455616133&s=170667a&w=0&h=ig7aqaF2HwKMOxlelB_lSjbycZZvhqFwVdw9yL2-mBc=",
    productName: "Excee Sneakers",
    productPrice: "260.00",
  },
  {
    id: 3,
    productImage:
      "https://media.istockphoto.com/photos/girl-in-sneakers-and-jeans-several-pairs-of-sports-shoes-and-legs-picture-id1158643545?k=20&m=1158643545&s=612x612&w=0&h=n1pzGr45_liJgyj6ZOZM3XzQCu8aRHOCQtNcVs9u2GM=",
    productName: "Air Max Motion 400",
    productPrice: "290.00",
  },
  {
    id: 4,
    productImage:
      "https://media.istockphoto.com/photos/pair-of-white-baby-shoes-on-embroidered-christening-white-dress-picture-id534002894?k=20&m=534002894&s=612x612&w=0&h=ZqkG8EFPQzjQ43cp7MC2EQ7SGnUyepyTX9zppPJ89_o=",
    productName: "Air Max Motion",
    productPrice: "300.00",
  },
  {
    id: 5,
    productImage:
      "https://media.istockphoto.com/photos/baby-shoes-picture-id970162892?k=20&m=970162892&s=612x612&w=0&h=N0ajAkJYbaj_6ShD9ht3UFlRoGA_HT-eNmwnOwoAJoE=",
    productName: "Nike Max 200",
    productPrice: "240.00",
  },
  {
    id: 6,
    productImage:
      "https://media.istockphoto.com/photos/yellow-sneakers-on-girl-legs-picture-id984335656?k=20&m=984335656&s=612x612&w=0&h=gv2q6TD74CHj_QxUNL8h90efHnXJrmWeOVp5jwDnLqo=",
    productName: "Excee Sneakers",
    productPrice: "260.00",
  },
  {
    id: 7,
    productImage:
      "https://media.istockphoto.com/photos/childrens-feet-in-pink-sneakers-standing-on-a-log-picture-id624112750?k=20&m=624112750&s=612x612&w=0&h=HRIsgHeYiHv7hhluD5q3cq8AxsCn2X7rNw9Js6aKjl4=",
    productName: "Pink Max Motion 2",
    productPrice: "290.00",
  },
  {
    id: 8,
    productImage:
      "https://media.istockphoto.com/photos/kids-with-colorful-shoes-children-footwear-picture-id859156946?k=20&m=859156946&s=612x612&w=0&h=S10S_SzQaL3yzeZgDhDR2zawfZCdHrQxnqVGvDHiTns=",
    productName: "Leather Loafer",
    productPrice: "300.00",
  },
  {
    id: 9,
    productImage:
      "https://media.istockphoto.com/photos/red-patent-leather-childrens-shoe-on-a-white-background-picture-id626089618?k=20&m=626089618&s=612x612&w=0&h=7YWekA_k3iDaicBFo_KWY6hKYCMR9sGcxwCrUiWi8Us=",
    productName: "Excee Loafer",
    productPrice: "260.00",
  },
  {
    id: 10,
    productImage:
      "https://media.istockphoto.com/photos/pink-babys-bootees-picture-id475722982?k=20&m=475722982&s=612x612&w=0&h=VaSnrlUVdqysPsKSBqFrkFIZsed58k7hQZ4CamYkeh8=",
    productName: "Excee Sneakers",
    productPrice: "260.00",
  },
];
export const getProducts = () => {
  return PRODUCTS;
};
export const getProduct = (id) => {
  return PRODUCTS.find((product) => product.id == id);
};
