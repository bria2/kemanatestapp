// This file contains shadow style which is used in the project

import { colors } from "./colors";

const shadows = {
  card: {
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
};

export { shadows };
