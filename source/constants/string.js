// This file contains all the string used in the project

export default {
  Title: "Footed In Fashion",
  addToCart: "Add To Cart",
  cartSummary: "Cart Summary",
  cart: "Cart",
  subtotal: "Sub Total",
  tax: "Tax",
  shipping: "Shipping",
  total: "Total",
  payNow: "Pay Now",
  orderPlaceSuccessfully: "Your order is placed successfully!",
  continueShopping: "Continue Shopping",
  clearAll: "Clear All",
};
