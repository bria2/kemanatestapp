// This file contains all the App name used in the project
export const SPLASH_SCREEN = "SPLASH_SCREEN";

export const PRODUCT = "PRODUCT";

export const CART_VIEW = "CART_VIEW";
export const PLACE_ORDER = "PLACE_ORDER";
