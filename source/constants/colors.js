// This file contains all the colors used in the project
const colors = {
  primary: "#ab698d",
  secondary: "#1B4633",
  white: "#FFFFFF",
  whiteShade: "#FAFAFA",
  black: "#000000",
  offWhite: "#f7f7f7",
};

export { colors };
