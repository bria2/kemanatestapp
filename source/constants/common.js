// This file contains all the constant used in the project
export const GLOBAL = {
  MIN_QTY: 1, //for the Minimum decrease quantity
  SHIPPING_FEE: `5.00`, //for the Shipping fee
  TAX: 1.23, //for the Tax
};
